//
//  Extensions.swift
//  KLARA UI
//
//  Created by Apple on 18/09/20.
//

import Foundation
import UIKit

extension UIView {
    
    //Apply gradient colors
    /*
    func addGradientLinear(colors: [UIColor] = [.blue, .white], locations: [NSNumber] = [0, 1], startPoint: CGPoint = CGPoint(x: 0.0, y: 0.0), endPoint: CGPoint = CGPoint(x: 1.0, y: 1.0), type : CAGradientLayerType = .axial){
        
        let gradient = CAGradientLayer()
        gradient.type = type
        gradient.frame.size = self.frame.size
        gradient.frame.origin = CGPoint(x: 0.0, y: 0.0)
        
        // Iterates through the colors array and casts the individual elements to cgColor
        // Alternatively, one could use a CGColor Array in the first place or do this cast in a for-loop
        gradient.colors = colors.map{ $0.cgColor }
        
        gradient.locations = locations
        gradient.startPoint = startPoint
        gradient.endPoint = endPoint
        gradient.cornerRadius = self.layer.cornerRadius
        // Insert the new layer at the bottom-most position
        // This way we won't cover any other elements
        self.layer.insertSublayer(gradient, at: 0)
        
    }*/
    
    func applyGradientLinear(colors: [UIColor] = [.blue, .white], locations: [NSNumber] = [0, 1], startPoint: CGPoint = CGPoint(x: 0.0, y: 0.0), endPoint: CGPoint = CGPoint(x: 1.0, y: 1.0), type : CAGradientLayerType = .axial) -> CAGradientLayer{
        
        let gradient = CAGradientLayer()
        gradient.type = type
        gradient.frame.size = self.frame.size
        gradient.frame.origin = CGPoint(x: 0.0, y: 0.0)
        
        // Iterates through the colors array and casts the individual elements to cgColor
        // Alternatively, one could use a CGColor Array in the first place or do this cast in a for-loop
        gradient.colors = colors.map{ $0.cgColor }
        
        gradient.locations = locations
        gradient.startPoint = startPoint
        gradient.endPoint = endPoint
        gradient.cornerRadius = self.layer.cornerRadius
        // Insert the new layer at the bottom-most position
        // This way we won't cover any other elements
        //self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
    
    func backBoxShadow() {
        layer.masksToBounds = false
        layer.shadowOffset = CGSize(width: 0, height: 0)
        if #available(iOS 11.0,tvOS 11.0, *) {
            layer.shadowColor = UIColor(named: "backShadowColor")!.cgColor
        } else {
            // Fallback on earlier versions
        }
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 20.0
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
      }
    
    func normalViewShadow() {
        layer.masksToBounds = false
        layer.shadowOffset = CGSize(width: -1, height: 1)
        if #available(iOS 11.0,tvOS 11.0, *) {
            layer.shadowColor = UIColor(named: "backShadowColor")?.cgColor
        } else {
            // Fallback on earlier versions
        }
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 7.0
//        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//        layer.shouldRasterize = true
//        layer.rasterizationScale = UIScreen.main.scale
    }
}


extension CALayer{
    
    @available(iOS 11.0,tvOS 11.0, *)
    func applyShadow(cornerRadius:CGFloat = 0, radius:CGFloat = 7,opacity:Float = 1, shadowColor : UIColor = UIColor(named: "backShadowColor")!,offset:CGSize = CGSize(width: -1, height: -1)){
        self.masksToBounds = false
        self.frame = bounds
        self.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        //self.shadowColor = UIColor(named: "backShadowColor")?.cgColor
        self.shadowColor = shadowColor.cgColor
        self.shadowOpacity = opacity
        self.shadowRadius = radius
        //self.shadowOffset = CGSize(width: -1, height: -1)
        self.shadowOffset = offset
    }
}

extension Bundle {
    func decode<T: Decodable>(_ type: T.Type, from file: String, dateDecodingStrategy: JSONDecoder.DateDecodingStrategy = .deferredToDate, keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy = .useDefaultKeys) -> T {
        guard let url = self.url(forResource: file, withExtension: nil) else {
            fatalError("Failed to locate \(file) in bundle.")
        }

        guard let data = try? Data(contentsOf: url) else {
            fatalError("Failed to load \(file) from bundle.")
        }

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = dateDecodingStrategy
        decoder.keyDecodingStrategy = keyDecodingStrategy

        do {
            return try decoder.decode(T.self, from: data)
        } catch DecodingError.keyNotFound(let key, let context) {
            fatalError("Failed to decode \(file) from bundle due to missing key '\(key.stringValue)' not found – \(context.debugDescription)")
        } catch DecodingError.typeMismatch(_, let context) {
            fatalError("Failed to decode \(file) from bundle due to type mismatch – \(context.debugDescription)")
        } catch DecodingError.valueNotFound(let type, let context) {
            fatalError("Failed to decode \(file) from bundle due to missing \(type) value – \(context.debugDescription)")
        } catch DecodingError.dataCorrupted(_) {
            fatalError("Failed to decode \(file) from bundle because it appears to be invalid JSON")
        } catch {
            fatalError("Failed to decode \(file) from bundle: \(error.localizedDescription)")
        }
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)

        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)

        return ceil(boundingBox.width)
    }
    
    func ploclize() -> String {
        return NSLocalizedString(self, comment: "")
    }
}


extension NSAttributedString {
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)

        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)

        return ceil(boundingBox.width)
    }
}
