//
//  GradientButton.swift
//  KLARA UI
//
//  Created by Apple on 27/09/20.
//

import UIKit
import Foundation

@IBDesignable class GradientButton: UIButton {
    
    enum style {
        case normal,disabled
    }
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    //let subLayer = CALayer()
    @available(iOS 11.0,tvOS 11.0, *)
    lazy var gradientLayer: CAGradientLayer! = {
        self.applyGradientLinear(colors:[UIColor(named: "Gradient_1_Primary")!,UIColor(named: "Dark_Primary")!])
    }()
    
    @IBInspectable var isDisabled: Bool = false {
        didSet {
            if #available(iOS 11.0, *) {
                self.setup(styled: isDisabled ? .disabled : .normal)
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    
    override init(frame:CGRect){
        super.init(frame: frame)
        if #available(iOS 11.0,tvOS 11.0, *) {
            self.setup()
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        if #available(iOS 11.0,tvOS 11.0, *) {
            self.setup()
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    @available(iOS 11.0,tvOS 11.0, *)
    private func setup(styled:style = .normal) {
        
        clipsToBounds = true
        layer.cornerRadius = self.frame.height/2
        layer.masksToBounds = false
        self.titleLabel?.font = UIFont().robotoStyle(style: .regular, size: .h5)
        
        if styled == .disabled{
            //self.isUserInteractionEnabled = false
            self.backgroundColor = UIColor(named: "GrayPale_Neutral")
            self.setTitleColor(UIColor(named: "Gray_Neutral"), for: .normal)
        }
        else{
            self.isUserInteractionEnabled = true
            self.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            //setShadowLayer()
            setGradientBackground()
            
        }
        self.addTarget(self, action: #selector(press_effect), for: .touchUpInside)
        
    }
    
    private func setGradientBackground() {
        
        //gradientLayer = self.applyGradientLinear(colors:[UIColor(named: "Gradient_1_Primary")!,UIColor(named: "Dark_Primary")!])
        if #available(iOS 11.0,tvOS 11.0, *) {
            gradientLayer.frame = bounds
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0,tvOS 11.0, *) {
            gradientLayer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: self.frame.height/2).cgPath
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0,tvOS 11.0, *) {
            gradientLayer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4).cgColor
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0,tvOS 11.0, *) {
            gradientLayer.shadowOpacity = 1
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0,tvOS 11.0, *) {
            gradientLayer.shadowRadius = 5
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0,tvOS 11.0, *) {
            gradientLayer.shadowOffset = CGSize(width: 0, height: 0)
        } else {
            // Fallback on earlier versions
        }
        
        if #available(iOS 11.0,tvOS 11.0, *) {
            layer.insertSublayer(gradientLayer!, at: 0)
        } else {
            // Fallback on earlier versions
        }
    }
    
    /*
    private func setShadowLayer(){
        
        subLayer.frame = bounds
        subLayer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: self.frame.height/2).cgPath
        subLayer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4).cgColor
        subLayer.shadowOpacity = 1
        subLayer.shadowRadius = 5
        subLayer.shadowOffset = CGSize(width: 0, height: 0)
        
        //layer.insertSublayer(subLayer, below: gradientLayer!)
    }*/
    
    func changeStyle(toStyle:style = .normal){
        //subLayer.removeFromSuperlayer()
        if #available(iOS 11.0,tvOS 11.0, *) {
            gradientLayer.removeFromSuperlayer()
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0,tvOS 11.0, *) {
            self.setup(styled: toStyle)
        } else {
            // Fallback on earlier versions
        }
    }
    
    @objc func press_effect(){
        //animateShadow()
        //animateGradient()
 
        let animation = CABasicAnimation(keyPath: "shadowOpacity")
        animation.fromValue = layer.shadowOpacity
        animation.toValue = 0.0
        animation.duration = 0.5
        
        let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
        if #available(iOS 11.0,tvOS 11.0, *) {
            gradientChangeAnimation.fromValue = gradientLayer!.colors
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0,tvOS 11.0, *) {
            gradientChangeAnimation.toValue = [UIColor(named: "Dark_Primary")!.cgColor,UIColor(named: "Dark_Primary")!.cgColor]
        } else {
            // Fallback on earlier versions
        };if #available(iOS 11.0,tvOS 11.0, *) {
            gradientChangeAnimation.toValue = [UIColor(named: "Dark_Primary")!.cgColor,UIColor(named: "Dark_Primary")!.cgColor]
        } else {
            // Fallback on earlier versions
        }

        let group = CAAnimationGroup()
        group.animations = [animation,gradientChangeAnimation]
        group.duration = 0.5
        group.isRemovedOnCompletion = true
        group.fillMode = CAMediaTimingFillMode.forwards
        //group.delegate = self
        if #available(iOS 11.0,tvOS 11.0, *) {
            self.gradientLayer!.add(group,forKey: "groupAnim")
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    /*
    func animateShadow(){
        let animation = CABasicAnimation(keyPath: "shadowOpacity")
        animation.fromValue = layer.shadowOpacity
        animation.toValue = 0.0
        animation.duration = 0.5
        self.gradientLayer!.add(animation, forKey: animation.keyPath)
        self.gradientLayer!.shadowOpacity = 1.0
    }
    
    func animateGradient(){
        
        
        let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
        gradientChangeAnimation.fromValue = gradientLayer!.colors
        
        gradientChangeAnimation.toValue = [UIColor(named: "Dark_Primary")!.cgColor,UIColor(named: "Dark_Primary")!.cgColor]
        //gradientChangeAnimation.toValue = [UIColor.red.cgColor,UIColor.yellow.cgColor]
        gradientChangeAnimation.duration = 0.5
        gradientChangeAnimation.fillMode = CAMediaTimingFillMode.forwards
        gradientChangeAnimation.isRemovedOnCompletion = true
        gradientLayer!.add(gradientChangeAnimation, forKey: "colorChange")
        
        /*
        let animation : CABasicAnimation = CABasicAnimation(keyPath: "colors")
        let toColors = [
            UIColor(red: 244/255, green: 88/255, blue: 53/255, alpha: 1).cgColor,
            UIColor(red: 196/255, green: 70/255, blue: 107/255, alpha: 1).cgColor
            ]
                animation.fromValue = gradientLayer!.colors
                animation.toValue = toColors
        animation.duration = 1.0
        animation.isRemovedOnCompletion = true
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
                //animation.delegate = self

        gradientLayer!.colors = toColors

        gradientLayer!.add(animation, forKey:"animateGradient")
         */
            }
    
    */
    
    
    
    
    
}
