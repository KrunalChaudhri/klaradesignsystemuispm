//
//  SecondaryButton.swift
//  KLARA UI
//
//  Created by Apple on 27/09/20.
//

import UIKit

@IBDesignable class SecondaryButton: UIButton, CAAnimationDelegate {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    enum style {
        case normal,disabled
    }
    
    let subLayer = CAShapeLayer()
    
    @IBInspectable var isDisabled: Bool = false {
        didSet {
            self.setup(styled: isDisabled ? .disabled : .normal)
        }
    }
    
    override init(frame:CGRect){
        super.init(frame: frame)
        self.setup()
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
//    override func prepareForInterfaceBuilder() {
//            setup()
//        }
    
    
    
    
    private func setup(styled : style = .normal) {
        
        clipsToBounds = false
        layer.cornerRadius = self.frame.height/2
        layer.masksToBounds = false
        self.titleLabel?.font = UIFont().robotoStyle(style: .regular, size: .h5)
        
        if styled == .disabled{
            self.isUserInteractionEnabled = false
            if #available(iOS 11.0,tvOS 11.0, *) {
                self.backgroundColor = UIColor(named: "GrayPale_Neutral")!
            } else {
                // Fallback on earlier versions
            }
            if #available(iOS 11.0,tvOS 11.0, *) {
                self.setTitleColor(UIColor(named: "Gray_Neutral")!, for: .normal)
            } else {
                // Fallback on earlier versions
            }
        }
        else{
            self.isUserInteractionEnabled = true
            self.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            if #available(iOS 11.0,tvOS 11.0, *) {
                self.setTitleColor(UIColor(named: "Primary_Primary")!, for: .normal)
            } else {
                // Fallback on earlier versions
            }
            setShadowLayer()
            
        }
        self.addTarget(self, action: #selector(press_effect), for: .touchUpInside)
        
    }
    
    private func setShadowLayer(){
        subLayer.frame = bounds
        subLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: self.frame.height/2).cgPath
        subLayer.fillColor = backgroundColor!.cgColor
        
        subLayer.shadowColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4).cgColor
        // subLayer.shadowPath = subLayer.path
        subLayer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        subLayer.shadowOpacity = 1.0
        subLayer.shadowRadius = 5
        //subLayer.shouldRasterize = true
        //subLayer.rasterizationScale = UIScreen.main.scale
        
        layer.insertSublayer(subLayer, at: 0)
    }
    
    func changeStyle(toStyle:style = .normal){
        subLayer.removeFromSuperlayer()
        self.setup(styled: toStyle)
    }
    
    @objc func press_effect(){
//        animateShadow()
//        animateBackgroundColor()
        
        
        self.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        
        let animation = CABasicAnimation(keyPath: "shadowOpacity")
        animation.fromValue = layer.shadowOpacity
        animation.toValue = 0.0
        //animation.duration = 0.5
        //self.subLayer.add(animation, forKey: animation.keyPath)
        //self.subLayer.shadowOpacity = 1.0
        
        let animcolor = CABasicAnimation(keyPath: "fillColor")
        animcolor.fromValue = backgroundColor!.cgColor
        if #available(iOS 11.0,tvOS 11.0, *) {
            animcolor.toValue = UIColor(named: "Dark_Primary")!.cgColor
        } else {
            // Fallback on earlier versions
        }
        //animcolor.duration = 0.5
        //animcolor.repeatCount = 0
        //animcolor.autoreverses = true
        //subLayer.add(animcolor, forKey: "fillColor")
        
        let group = CAAnimationGroup()
        group.animations = [animation,animcolor]
        group.duration = 0.5
        group.isRemovedOnCompletion = true
        group.delegate = self
        self.subLayer.add(group,forKey: "groupAnim")
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if #available(iOS 11.0,tvOS 11.0, *) {
            self.setTitleColor(UIColor(named: "Primary_Primary")!, for: .normal)
        } else {
            // Fallback on earlier versions
        }
    }
    
    /*
    func animateShadow(){
        let animation = CABasicAnimation(keyPath: "shadowOpacity")
        animation.fromValue = layer.shadowOpacity
        animation.toValue = 0.0
        animation.duration = 0.5
        self.subLayer.add(animation, forKey: animation.keyPath)
        self.subLayer.shadowOpacity = 1.0
    }
    
    func animateBackgroundColor(){
        
        self.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            
            let animcolor = CABasicAnimation(keyPath: "fillColor")
            animcolor.fromValue = backgroundColor!.cgColor
            animcolor.toValue = UIColor(named: "Dark_Primary")!.cgColor
            animcolor.duration = 0.5
            animcolor.repeatCount = 0
            animcolor.autoreverses = true
            subLayer.add(animcolor, forKey: "fillColor")
            

//        UIView.animate(withDuration: 0.2) {
//            self.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
//        } completion: { (finish) in
//            self.setTitleColor(UIColor(named: "Primary_Primary")!, for: .normal)
//        }
        
    }*/
    
    
    
}
