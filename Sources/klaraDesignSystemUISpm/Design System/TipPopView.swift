//
//  TipPopView.swift
//  RegioApp
//
//  Created by Piyush Agrawal on 18/10/20.
//

import UIKit

class TipPopView: UIView {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    var view:UIView!
    
    var close_press: (()->Void)?
    
//    var  lbl1_text : String?
//    var lbl2_text : String?
    
    @IBOutlet  weak var lbl1: UILabel!
    @IBOutlet  weak var lbl2: UILabel!
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aCoder: NSCoder) {
        super.init(coder: aCoder)!
        setup()
    }
    
    func setup() {
        view = self.loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //view.translatesAutoresizingMaskIntoConstraints = false
        //addSubview(view)
        
//        lbl1.text = lbl1_text ?? ""
        lbl1.font = UIFont().robotoStyle(style: .bold, size: .h5)
//        lbl2.text = lbl2_text ?? ""
        lbl2.font = UIFont().robotoStyle(style: .regular, size: .h5)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        return UINib(nibName: "TipPopView", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }
    
    @IBAction private func close_tapped(){
        if let buttonAction = self.close_press {
            buttonAction()
        }
    }
    
     func updateConstraintslayout() {
        
        self.layoutIfNeeded()
    }
    
}
