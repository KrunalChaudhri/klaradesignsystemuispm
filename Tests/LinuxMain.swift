import XCTest

import klaraDesignSystemUISpmTests

var tests = [XCTestCaseEntry]()
tests += klaraDesignSystemUISpmTests.allTests()
XCTMain(tests)
